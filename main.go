package main

import (
	"bufio"
	"fmt"
	"os"
)

const (
	tt string = "input/tt.txt"
	sl string = "input/sue_list.txt"
)

func main() {
	var (
		answer int
	)

	// no need to get flags just load the files right away
	sueList, tickTape, err := readFiles()
	if err != nil {
		fmt.Printf("ERROR:\n    %v\n", err)
	}

	answer = searchSueList(sueList, tickTape)

	fmt.Println("====================================")
	fmt.Printf("Answer: %v\n", answer)
}

// searchSueList - 
func searchSueList(sueList []string, tickerData []string) (sueNum int) {
	
	return sueNum
}

// readFiles -
func readFiles() (sueList []string, tickerData []string, err error) {

	slF, err := os.Open(sl)
	if err != nil {
		return sueList, tickerData, err
	}
	defer slF.Close()

	ttF, err := os.Open(tt)
	if err != nil {
		return sueList, tickerData, err
	}
	defer ttF.Close()

	scn := bufio.NewScanner(slF)
	for scn.Scan() {
		tmpLn := scn.Text()
		sueList = append(sueList, tmpLn)
	}

	scn = bufio.NewScanner(ttF)
	for scn.Scan() {
		tmpLn := scn.Text()
		tickerData = append(tickerData, tmpLn)
	}

	return sueList, tickerData, err
}
